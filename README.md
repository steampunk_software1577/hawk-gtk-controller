# Hawk GTK Controller
This is the main repository for Hawk GTK Controller.
A Python Program which helps you to control your hawk module.
For further details about hawk please refer to this repository:

https://gitlab.com/steampunk_software1577/hawk-vision

# What does it do?
Using this program you can control and configure your hawk modules.
In a nutshell hawk is a collection of scripts which enables FRC teams to develop image processing algorithms very easily.
Using Hawk controller you can:

-Control IP address 

-Change password for your processor computer

-Calibrate cameras settings

-Choose grip files and scripts

-Debug using the console

-Manage config files, scripts and grip files

Note this program will only work on unix and linux systems because it requires GTK.
There is a qt version hosted at:

https://gitlab.com/steampunk_software1577/hawk-qt-controller

Windows user can use the Qt version or run this version via an emulation layer(like WSL)

# Basic usage
If you use the official images then
enter root@<address> and press connect
password is hawk

Go to camera control enter camera id and change the settings to your liking, press calibrate when you are done.

Now install GRIP
https://github.com/WPIRoboticsProjects/GRIP

After that, open GRIP select IP camera and enter the link
http://<processor computer address>:<camera port>/steam.mjpg
(for camera 0 the port is 1181, for camera 1 the port is 1182 etc)

now just create your gripfile and make sure it ends with a contour filter, or use our prebuilt gripfile and set the HSV thersholds your self.
generate it as a python file.

Notice that the grip file must end with filter contours

Then write your script.

Documentation on scripting will be up soon.

Go to file manager and push your grip file and your script.

then go to settings configure the settings and choose the grip file and script you would like to use.


# Dependencies
Python3

GTK

paramiko

# Note
The project is very early stages
Any help is welcomed

Also this project is a part of the hawk-vision project
https://gitlab.com/steampunk_software1577/hawk-vision


# Contact
For any request or question you can either submit an issue or email at:

oren_daniel@protonmail.com --original author

steampunk.software@gmail.com

