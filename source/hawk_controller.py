#!/usr/bin/python3

'''
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib, GObject, WebKit2
import sys
import paramiko
import json
import time

REFRESH_RATE = 500
PORT_ZERO = 1181

builder = Gtk.Builder()
builder.add_from_file("ui/layout.glade")
browser = WebKit2.WebView()
browser.set_editable(False)
connected = False
address = ''
ssh_client = None

def connection_lost(logger_name):
	global ssh_client
	global connected
	global address
	ssh_client = None
	connected = False
	address = ''
	get_object(logger_name).set_text("lost connection!")
	get_object('button_connect').get_child().set_text('disconnected')



def get_object(name):
	return builder.get_object(name)

def populate_combo(combo, str_array):
	get_object(combo).remove_all()
	for item in str_array:
		get_object(combo).append_text(item)

def update_file_manager():
	try:
		ls_scripts = ssh_client.exec_command("ls ~/.hawk/scripts")[1].read()
		ls_grips = ssh_client.exec_command("ls ~/.hawk/grips")[1].read()
		ls_scripts = ls_scripts.decode('utf-8').strip('\n').splitlines()
		ls_grips = ls_grips.decode('utf-8').strip('\n').splitlines()
		populate_combo('combo_remote_scripts', ls_scripts)
		populate_combo('combo_scripts', ls_scripts)
		populate_combo('combo_remote_grips', ls_grips)
		populate_combo('combo_grips', ls_grips)
	except Exception as e:
		connection_lost('label_fm_logger')
		print(e)

def delete_file(reletive_path):
	try:
		ssh_client.exec_command("rm ~/.hawk/" +  reletive_path)
		get_object('label_fm_logger').set_text('done')
	except Exception as e:
		connection_lost('label_fm_logger')
		print(e)

def destroy(arg):
	Gtk.main_quit()

def send_file(local_path, remote_path):
	global ssh_client
	if ssh_client != None:
		try:
			with open(local_path) as file:
				file_contant = "".join(i for i in file.readlines()).replace('"', '\\"').replace("'", "\'")
				ssh_client.exec_command('echo "' + file_contant + '" > ' + remote_path)
			get_object('label_fm_logger').set_text('')
		except Exception as e:
			get_object('label_fm_logger').set_text(str(e))
	else:
		get_object('label_fm_logger').set_text("you are disconnected")

def digits_only(entry_name):
	text = get_object(entry_name).get_text().strip()
	get_object(entry_name).set_text(''.join([i for i in text if i in '0123456789']))

def refresh():
	global ssh_client
	try:
		if connected:
			_, stdout, _ = ssh_client.exec_command("cat ~/.hawklog")
			lines = stdout.read().decode('utf-8').splitlines()
			text_buffer = get_object('text_view_console').get_buffer()
			append = '\n'.join(lines[i] for i in range(text_buffer.get_line_count()-1, len(lines))) + '\n'
			if append.strip('\n') != '':
				text_buffer.insert(text_buffer.get_end_iter(), append)
			GLib.timeout_add(REFRESH_RATE, refresh)
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def button_connect_clicked(arg):
	global connected
	global ssh_client
	global address
	try:
		if not connected:
			ssh_client = paramiko.SSHClient()
			ssh_client.load_system_host_keys()
			address = get_object('entry_ssh').get_text()
			username = address.split('@')[0]
			hostname = address.split('@')[1]
			address = hostname
			password = get_object('entry_password').get_text()
			ssh_client.load_system_host_keys()
			ssh_client.connect(hostname, username=username, password=password)
			ssh_client.exec_command("mkdir ~/.hawk")
			ssh_client.exec_command("mkdir ~/.hawk/grips")
			ssh_client.exec_command("mkdir ~/.hawk/scripts")
			ssh_client.exec_command("mkdir ~/.hawk/cameras")
			connected = True
			get_object('button_connect').get_child().set_text('connected')
			get_object('label_control_logger').set_text('')
			update_file_manager()
			text_buffer = get_object('text_view_console').get_buffer()
			text_buffer.delete(text_buffer.get_start_iter(), text_buffer.get_end_iter())
			GLib.timeout_add(REFRESH_RATE, refresh)
		else:
			ssh_client = None
			connected = False
			get_object('button_connect').get_child().set_text('disconnected')
			get_object('label_control_logger').set_text('')
			get_object('label_settings_logger').set_text('')
			get_object('label_fm_logger').set_text('')
			populate_combo('combo_remote_scripts', [])
			populate_combo('combo_scripts', [])
			populate_combo('combo_remote_grips', [])
			populate_combo('combo_grips', [])
	except Exception as e:
		get_object('label_control_logger').set_text('cannot connect')
		connected = False
		ssh_client = None
		get_object('button_connect').get_child().set_text('disconnected')
		print(e)

def button_change_password_clicked(arg):
	try:
		password1 = get_object('entry_new_password').get_text()
		password2 = get_object('entry_confirm').get_text()
		if password1 == password2:
			ssh_client.exec_command("~/hawk-vision/scripts/change_password.sh " + password1)
			get_object('label_control_logger').set_text("password changed")
		else:
			get_object('label_control_logger').set_text("passwords don't match")
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def entry_camera_number_changed(arg):
	global address
	digits_only('entry_camera_number')
	port = str(int(get_object('entry_camera_number').get_text()) + PORT_ZERO)
	url = 'http://' + address + ':' + port
	browser.load_uri(url)
	
def button_calibrate_camera_clicked(arg):
	try:
		port = get_object('entry_camera_number').get_text()
		ssh_client.exec_command('~/hawk-vision/scripts/config_camera.sh ' + port)
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def button_refresh_camera_clicked(arg):
	browser.reload()

def button_reboot_clicked(arg):
	global ssh_client
	try:
		ssh_client.exec_command("reboot")
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def entry_team_number_changed(arg):
	digits_only('entry_team_number')

def entry_width_changed(arg):
	digits_only('entry_width')

def entry_height_changed(arg):
	digits_only('entry_height')

def entry_fps_changed(arg):
	digits_only('entry_fps')

def button_save_clicked(arg):
	global ssh_client
	try:
		script_file = get_object('combo_scripts').get_active_text().strip() or ''
		grip_file = get_object('combo_grips').get_active_text().strip() or ''
		pipeline_name = (get_object('entry_pipeline_name').get_text()) or 'vision'
		team_number = (get_object('entry_team_number').get_text()) or -1
		width = get_object('entry_width').get_text() or 160
		height = get_object('entry_height').get_text() or 120
		fps = get_object('entry_fps').get_text() or 30
		if team_number == -1:
			get_object('label_settings_logger').set_text('please select team number')
			return
		if script_file == '' or grip_file == '':
			get_object('label_settings_logger').set_text('please select grip file and script file')
			return
		contant = {
			'grip_file':str(grip_file),
			'script_file':str(script_file),
			'pipe_name':str(pipeline_name),
			'team_number':int(team_number),
			'width':int(width),
			'height':int(height),
			'fps':int(fps)
		}
		json_file = json.dumps(contant)
		ssh_client.exec_command("echo '"  + json_file + "' > ~/.hawk/config.json")
		get_object('label_settings_logger').set_text("done!")
	except Exception as e:
		connection_lost('label_settings_logger')
		print(e)

def entry_camera_reset_changed(arg):
	digits_only('entry_camera_reset')

def button_reset_camera_clicked(arg):
	delete_file('cameras/' + get_object('entry_camera_reset').get_text() + '.json')
	update_file_manager()

def button_script_push_clicked(arg):
	local_path = get_object('fc_script').get_filename()
	file_name = local_path.split('/')[len(local_path.split('/'))-1]
	send_file(local_path, '~/.hawk/scripts/' + file_name)
	update_file_manager()

def button_script_delete_clicked(arg):
	delete_file('scripts/' + get_object('combo_remote_scripts').get_active_text())
	update_file_manager()

def button_grip_push_clicked(arg):
	local_path = get_object('fc_grip').get_filename()
	file_name = local_path.split('/')[len(local_path.split('/'))-1]
	send_file(local_path, '~/.hawk/grips/' + file_name)
	update_file_manager()

def button_grip_delete_clicked(arg):
	delete_file('grips/' + get_object('combo_remote_grips').get_active_text())
	update_file_manager()

def button_update_software_clicked(arg):
	try:
		ssh_client.exec_command('cd hawk-vision/;  git fetch --all; git reset --hard origin/master; cd ~')
		get_object('label_control_logger').set_text("pulled software")
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def button_change_dns_clicked(arg):
	try:
		name = get_object('entry_change_dns').get_text()
		ssh_client.exec_command('hostnamectl set-hostname ' + name)
		get_object('label_control_logger').set_text('DNS changed please reboot to commit')	
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def button_set_ip_clicked(arg):
	try:
		address = get_object('entry_address').get_text() 
		ssh_client.exec_command('~/hawk-vision/scripts/set_static_ip.sh ' + address)
		get_object('label_control_logger').set_text('changed address please reboot and log with ip address')
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

def button_set_dhcp_clicked(arg):
	try:
		ssh_client.exec_command('~/hawk-vision/scripts/remove_static_ip.sh')
		get_object('label_control_logger').set_text('changed to dhcp please reboot')	
	except Exception as e:
		connection_lost('label_control_logger')
		print(e)

handler = {
	'destroy':destroy,
	'button_connect_clicked':button_connect_clicked,
	'button_set_ip_clicked':button_set_ip_clicked,
	'button_set_dhcp_clicked':button_set_dhcp_clicked,
	'button_change_password_clicked':button_change_password_clicked,
	'entry_camera_number_changed':entry_camera_number_changed,
	'button_calibrate_camera_clicked':button_calibrate_camera_clicked,
	'button_reboot_clicked':button_reboot_clicked,
	'entry_team_number_changed':entry_team_number_changed,
	'entry_width_changed':entry_width_changed,
	'entry_height_changed':entry_height_changed,
	'entry_fps_changed':entry_fps_changed,
	'button_save_clicked':button_save_clicked,
	'entry_camera_reset_changed':entry_camera_reset_changed,
	'button_reset_camera_clicked':button_reset_camera_clicked,
	'button_script_push_clicked':button_script_push_clicked,
	'button_script_delete_clicked':button_script_delete_clicked,
	'button_grip_push_clicked':button_grip_push_clicked,
	'button_grip_delete_clicked':button_grip_delete_clicked,
	'button_update_software_clicked':button_update_software_clicked,
	'button_change_dns_clicked':button_change_dns_clicked,
	'button_refresh_camera_clicked':button_refresh_camera_clicked,
}

builder.connect_signals(handler)
get_object('window').show_all()
get_object('web_view').add(browser)
browser.show()
Gtk.main()
